# **Cahier de laboratoire UE Projet Roscoff 2019** 
### _Maëlle BODO & François LEROY & Simon RONDEAU_

## **Thème du projet : génomique et phylogénie des cryptophytes**

### **SOMMAIRE**

Lundi 02/12/2019
*  Construction d'un arbre phylogénétique de référence en utilisant RaxML

Mardi 03/12/2019

*  Visualisation de l'arbre de référence obtenu
*  Transformation de l'alignement de référence en format phylip avec python
*  Récupération des paramètres du modèle en utilisant RAxML
*  Alignement des OTUs sur l'alignement de référence en utilisant PaPaRa

Mercredi 04/12/2019

*  Extraction des OTU alignés à l'aide de l'option _--split_ dans EPA-ng
*  Mise en place phylogénétique des UTO sur l'arbre de référence en utilisant EPA-ng
*  Analyse des placements avec GAPPA

Jeudi 05/12/2019

### **LUNDI 02/12/2019**

#### _Contruction d'un arbre phylogénétique de référence en utilisant RaxML_

La construction de cet arbre est réalisée à partir d'un fichier fasta contenant un alignement de séquence nommé "Crypto.fst" ainsi que du programme raxml-8.2.12.
Celui-ci à besoin d'être appelé au sein du cluster à travers la ligne de code suivante : source $CONDA3/activate raxml-8.2.12.

L'utilisation d'un manuel du programme raxml-8.2.12 à permis le choix d'utilisation de différents paramètres :
*  -m : Sélection du modèle utilisé, ici "GTRCAT" (modèle nucléotidique).
*  -f : Sélection de l'algorythme utilisé, ici "a" étant une analyse Bootstrap rapide et recherchant l'arbre le plus performant. 
*  -# : Spécification du nombre de run réalisés au sein du programme.
*  -p : Spécification du nombre aléatoire afin d'obtenir le meilleur arbre phylogénique, permetant de reproduire les résultats.
*  -x : Spécification d'un un nombre entier aléatoire afin d'obtenir les meilleurs bootstrap, permettant de reproduction les résultats (random seed).
*  -s : Sélection du fichier fasta d'alignement à partir duquel l'arbre sera construit.
*  -n : Sélection du nom de fichier de sortie.

_Remarque :_
Le script réalisé sur nano (MobaXterm) n'a pas pu être utilisé directement car malgré l'absence d'erreur en son sein, la console ne semblait pas pouvoir trouver les fichiers nécessaires à son fonctionnement.
Ce même script à été transféré, ouvert dans une application notepad, ré-enregistré puis réutilisé au sein de la console où celui-ci à fonctionné normalement.
L'origine des erreures observées au départ est sans explications, potentiellement en rapport avec l'outil "nano" permettant l'édition de script directement sur la console.

_Script Final utilisé_     "raxml-tree-building-n_2.qsub"
~~~
#!/bin/bash
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -q short.q
#$ -pe thread 4

source $CONDA3/activate raxml-8.2.12

raxmlHPC-PTHREADS -T 4 -f a -x 12345 -p 54321 -# 100 -m GTRCAT -s Crypto.fst -n Cryptophyta_REF_tree
~~~

### **MARDI 03/12/2019**

#### _Visualisation de l'arbre de référence obtenu_

Le programme réalisé le 02/12 a permis d'obtenir 4 types de fichiers :

*  RAxML_info : Ce fichier contient les informations sur l'argorithme qui a été utilisé. Il indique ce que RAxML a fait et est toujours écrit indépendamment de l'option de ligne de commande. De plus, il contient des informations sur tous les autres fichiers de sortie qui ont été écrits par l'exécution.
*  RAxML_bootstrap.exampleRun : Ce fichier contient tous les arbres finalbootstrap.
*  RAxML_bipartitions.exampleRun : Ce fichier contient le meilleur arbre trouvé avec des valeurs de confiance de 0 à 100 sur les noeuds.
*  RAxML_bipartitionsBranchLabels.exampleRun : Ce fichier est le même que le précédent mais, les valeurs de support sont affichées en tant qu'étiquettes de branche et non de noeud. 
*  RAxML_bestTree.exampleRun : Ce fichier contient l'arbre le plus performant d'une analyse approfondie. Il ne possède aucune valeur de bootstraps.

Le fichier conservé pour le rapport est le fichier _"bipartitionsBranchLabels"_ car c'est le plus complet, il contient :
*  Les valeurs du bootstrap
*  La longueur des branches* 

L'arbre sera modifié et mis en page grâce au logiciel iTOL (Interaction Tree Of Life).

#### _Transformation de l'alignement de référence en format phylip avec python_

Pour la suite de notre programme, l'alignement de référence, en format fasta, nécessite d'être converti en format phylip. 
Pour cela, un script (ci-dessous) a été réalisé dans python puis lancé sur MobaXterm, en indiquant les fichiers d'entrée et de sortie


```python
from Bio import AlignIO

def transform_format (fasta_file):

	input_handle = open(fasta_file, "r")
	output_handle = open("Ref_align_phy.phy", "w")

	alignments = AlignIO.parse(input_handle, "fasta")
	AlignIO.write(alignments, output_handle, "phylip-relaxed")

	output_handle.close()
	input_handle.close()

transform_format("Cryptophyta_REF_alignment.fasta")
```
### _Récupération des paramètres du modèle en utilisant RAxML_

Objectif : Pour la suite de notre programme, il faut récupérer les paramètres de notre modèle.
Dans notre premier script, _m_ indique le modèle d'évolution dans RaxML, il détermine si les sites évoluent à la même vitesse ou non dans l'alignement.
En réalité, les sites n'évoluent jamais à la même vitesse donc il est important d'en tenir compte. Le script qui suit va nous permettre de ré-estimer
les paramètres du modèle pour les avoir sur un fichier en utilisant l'arbre qu'il a précédemment construit. Ceci est possible en remplaçant la fonction
_f a_ par la fonction _f e_ et en lui donnant le meilleur arbre en fichier d'entrée.

_Script final utilisé_      "raxml-tree-building-n_param.qsub"
~~~
!/bin/bash
$ -S /bin/bash
$ -V
$ -cwd
$ -q short.q
$ -pe thread 4

source $CONDA3/activate raxml-8.2.12

raxmlHPC-PTHREADS -T 4 -f e -t RAxML_bestTree.Cryptophyta_REF_tree -m GTRCAT -s Crypto.fst -n Cryptophyta_REF_param
~~~

Ce script a permis d'obtenir 4 types de fichier:
*  RAxML_info : Ce fichier contient les informations sur l'argorithme qui a été utilisé. Il indique ce que RAxML a fait et est toujours écrit indépendamment de l'option de ligne de commande. De plus, il contient des informations sur tous les autres fichiers de sortie qui ont été écrits par l'exécution.
*  RAxML_log : Ce fichier imprime l'heure, la valeur de vraisemblance de l'arbre courant et le numéro du fichier de points de contrôle (si l'utilisation de points de contrôle a été spécifiée) après chaque fichier itération de l'algorithme de recherche. Dans la dernière ligne, il contient également la valeur finale de vraisemblance de la topologie finale de l'arbre.
*  RAxML_result : Ce fichier contient la topologie finale de l'arbre de l'exécution en cours. 
*  RAxML_binaryModelParameters : Ce fichier contient divers paramètres tel que le taux de substitution entre nucléotides, la proportion de gap, la proportion des 4 nucléotides dans l'alignement, etc.


#### _Alignement des OTUs sur l'alignement de référence en utilisant PaPaRa_

Objectif : cette étape permet de réaliser un alignement des OTUs inconnus sur les séquences de référence afin de pouvoir par la suite, réaliser un nouvel arbre comprenant les séquences de référence et les OTUs.
Cette étape à été réalisée en double : une fois pour le jeu de données DataSet1 et une autre pour le jeu de données DataSet2.

L'utilisation du site internet https://cme.h-its.org/exelixis/web/software/papara/index.html à permis le choix d'utilisation de différents paramètres :
*  -j : Spécification du nombre de thread (= fil d'exécution)
*  -t : Sélection du fichier contenant l'arbre de référence
*  -s : Sélection du fichier phylip qui contient l'alignement de référence
*  -q : Sélection du fichier fasta contenant les OTUs inconnus non alignés
*  -n : Sélection du nom de fichier de sortie
*  -r : Fonction qui permet d'empêcher de rajouter des gaps dans l'alignement de départ.

_Script final utilisé_      "align_dataset1.qsub" ou "align_dataset2.qsub"
~~~
!/bin/bash
$ -S /bin/bash
$ -V
$ -cwd
$ -q long.q
$ -pe thread 8

papara -j 8 -t RAxML_bestTree.Cryptophyta_REF_tree -s Ref_align_phy.phy -q Dataset_1.fasta -n align_D1 -r
~~~

Ce programme a donné 2 types de fichiers de sorties :
*  papara_log : Ce fichier contient un résumé de la ligne de commande lancée et de ce qu'il a fait.
*  papara_alignment : Ce fichier contient l'alignement.

### **MERCREDI 04/12/2019**

#### _Extraction des OTU alignés à l'aide de l'option --split dans EPA-ng_

Objectif : séparation des OTUs alignés des séquences de référence.
Cette étape à été réalisée en double : une fois pour le jeu de données DataSet1 et une autre pour le jeu de données DataSet2.

L'utilisation du site internet https://github.com/Pbdas/epa-ng#setting-the-model-parameters à permis de déterminer les fichiers nécessaires soit :
* 

_Script final utilisé_      "split_dataset1.qsub" ou "split_dataset2.qsub"

~~~
!/bin/bash
$ -S /bin/bash
$ -V
$ -cwd
$ -q short.q

source $CONDA3/activate epa-ng-0.3.6

epa-ng --split Ref_align_phy.phy papara_alignment_align_D1.phy
~~~

Fichiers de sortie :
* query.fasta : Fichier contenant les OTUs alignés (format fasta)
* reference.fasta : Fichier contenant les séquences de référence (format fasta)

#### _Mise en place phylogénétique des UTO sur l'arbre de référence en utilisant EPA-ng_

Objectif : 

L'utilisation du site internet https://github.com/Pbdas/epa-ng#setting-the-model-parameters à permis le choix d'utilisation de différents paramètres :
* -T : Spécification du nombre de thread (= fil d'exécution)
* -t : Fichier de l'arbre de référence (fichier newick)
* -s : Fichier contenant les séquences de référence (format fasta)
* -q : Fichier contenant les OTUs inconnus (format fasta)
* --model : Fichier contenant les paramètres du modèle

_Script final utilisé_      "placement_dataset1" ou "placement_dataset2"

~~~
!/bin/bash
$ -S /bin/bash
$ -V
$ -cwd
$ -q long.q
$ -pe thread 8

source $CONDA3/activate epa-ng-0.3.6

epa-ng -T 8 -t RAxML_bestTree.Cryptophyta_REF_tree -s reference_1.fasta -q query_1.fasta --model RAxML_info.Cryptophyta_REF_param
~~~

Fichiers de sortie :
* epa_info.loj : Ce fichier contient un résumé de la ligne de commande lancée et de ce qu'il a fait.
* epa_result.jplace : Ce fichier contient les arbres de références avec les placements des OTUs

Nous avons donc obtenu deux fichiers .jplace: un pour chaque jeu de donnée.

#### _Analyse des placements avec GAPPA_ 

Objectif : Calculer et visualiser la dispersion des OTUs sur l'arbre de référence

L'utilisation d'un manuel du programme GAPPA (http://gappa.gforge.inria.fr/gappa.pdf et https://github.com/lczech/gappa/wiki)
à permis le choix d'utilisation de différents paramètres :
* analyze : permet d'indiquer le(s) type(s) d'analyse(s) que l'on veut effectuer sur les deux fichiers .jplace (ici dispersion)
* --jplace-path: indique le chemin jusqu'aux fichiers .jplace
* --write: indique le format des arbres et fichiers obtenus
* --threads: indique le nombre de cores utilisés
* --mass-norme: indique la normalisation des données (ici relative)
* --edge-values: précise les valeurs à calculer
* --tree-file-prefix: précise le nom du fichier crée
* --svg-tree-shape: forme de l'arbre obtenu


_Script final utilisé_      
~~~
#!/bin/bash
#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -q long.q
#$ -pe thread 8

source $CONDA3/activate gappa-0.5.1

gappa analyze dispersion --jplace-path /projet/fr2424/stage/stage21/ue_projet2019/script/ --write-newick-tree --threads 8 --mass-norm relative --edge-values both --write-svg-tree --tree-file-prefix dispersion_masses --svg-tree-shape rectangular
~~~

Fichiers de sortie :
* disper_imbalancesimbalances_var_log.newick
* dispersion_massesmasses_var_log.newick

Ces fichiers .newick permettent de mettre en évidence la variance de la répartition des OTUs en fonction de s'ils appartiennent
au premier ou au second jeu de données. Ils sont exploitables directement avec le site iTOL. 
La variance des Edge Masses indique pour chaque branche s'il y a une différence de répartition des OTUs marins et dulcicole. 
La variance des Edge Imbalances va quant à elle indiquer, pour chaque ramification dichotomique, si une branche contient une différence 
dans l'abondance des OTUs marins et dulcicoles ou s'il elle contient la même proportion de chaque.
Nous avons décidé d'utiliser une échelle log afin d'observer plus finement les variances et d'éviter la polarisation de la variance par
peu d'espèces.

### **JEUDI 05/12/2019**

Travail sur les figures et interprétations.
